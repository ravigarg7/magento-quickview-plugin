/**
 *  Master Software Solutions
 *
 *  Mss_Quickview
 *
 * @author    <damanjit kaur> <daman1919@hotmail.com>
 * @category  Master Software Solutions
 * @package   Mss_Quickview
 *
 */
<?php   
class Mss_Quickview_Block_Index extends Mage_Core_Block_Template
{   
protected $_productCollection;
protected function _getProductCollection() {
    if (is_null($this->_productCollection)) {
        $collection = Mage::getModel('catalog/product')
            ->getCollection('*')
            ->addAttributeToSelect('*');
        Mage::getModel('catalog/layer')->prepareProductCollection($collection);
        $this->_productCollection = $collection;
    }
    return parent::_getProductCollection();
}




}