<?php 
/**
 *  Master Software Solutions
 *
 *  Mss_Quickview
 *
 * @author    <damanjit kaur> <daman1919@hotmail.com>
 * @category  Master Software Solutions
 * @package   Mss_Quickview
 *
 */
 ?>
 
<?php
class Mss_Quickview_IndexController extends Mage_Core_Controller_Front_Action{
    public function IndexAction() {
      if(Mage::getStoreConfig('quickview/frontend/enable')==1){
	  $this->loadLayout();   
      $this->renderLayout(); 
	  }
	  
    }
	public function prodAction()
	{
	
	$a = $this->getRequest()->getParams();
	Mage::register('prod_id', $a);
	$html = Mage::getSingleton('core/layout')->createBlock('core/template')
            ->setTemplate('quickview/test.phtml')->toHtml();
			
	$this->getResponse()->setBody(Mage::helper('core')->jsonEncode($html));
	}
	
	
	
}